import { Component } from '@angular/core';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],

})
export class HomePage {

  status = "";

  constructor(
    private inAppBrowser: InAppBrowser,
    private http: HttpClient
    ) { }

  async initPayment() {
    this.status = "Initializing...";

    let url = "https://travelamenity.com/mobileapp/admin/payment//prepare_order?cartid=dfghjk4567fgh";

    // Below code for api posting

    // let postParams = { amount: 50, billing_name: 'dkumar' };
    // let headers = new HttpHeaders()
    // let data = await this.http.get(url, { headers: headers }).pipe(map(data => { return data })).toPromise();
    // console.log("Posted",data);

    var options = "location=yes,beforeload=yes,toolbar=no,hideurlbar=yes,hidenavigationbuttons=yes";
    let browser = this.inAppBrowser.create(url, '_blank', options);    //This will open link in InAppBrowser

    browser.on('loadstop').subscribe(event => {
      this.status = "Processing...";
      console.log("event::",event);
      
      var cancelUrl = 'https://www.travelamenity.com/mobileapp/admin/payment/cancel';
      var successUrl="https://www.travelamenity.com/mobileapp/admin/payment/success";

      if (event.url==cancelUrl){
        this.status = "Transaction Cancelled...";
        browser.close();     
      }

      if (event.url==successUrl){
        this.status = "Transaction Success...";
        browser.close();     
      }
       

     
    });

    
  }

}
